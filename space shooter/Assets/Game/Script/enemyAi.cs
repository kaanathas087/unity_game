﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyAi : MonoBehaviour {

    //speed
    [SerializeField]
    private float _speed = 5.0f;
    // Use this for initialization


    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        //move down

        transform.Translate(Vector3.down * _speed * Time.deltaTime);


        // when come to bottom off the screen
 if (transform.position.y < -4.6f)
        {
            float randomposi = Random.Range(-8.5f, 8.5f);
            transform.position = new Vector3(randomposi, 4.6f, 0);
        }

       }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "laser")
        {
            Destroy(other.gameObject);
            Destroy(this.gameObject);
        }

        if(other.name == "player")
        {
            Player pa = other.GetComponent<Player>();
            pa.damage();
            Destroy(this.gameObject);
        }



    }
}
