﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class powerup : MonoBehaviour {

    [SerializeField]
    private float _speed = 5.0f;

    [SerializeField]
    private int powerupid;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector3.down * _speed * Time.deltaTime);
    }

   private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log(other.name);
        if (other.name == "player")
        {
  Player pa = other.GetComponent<Player>();
           if (pa != null)
            {
                if (powerupid == 0)
                {
                    //enable shootup
                    pa.powerupon();

                }
                else if (powerupid == 1)
                {
                    //enable speedup
                    //  pa.canspeedup = true;
                    pa.speedboot();
                }

            }
            Destroy(this.gameObject);

        }

    }
}
