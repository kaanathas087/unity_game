﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    [SerializeField]
    private float _firerate=.25f;
    [SerializeField]
    private float _canfire = 0.0f;
    [SerializeField]
    private GameObject _laserprefade;
    [SerializeField]
    private float _speed = 5.0f;
    [SerializeField]
    private GameObject _trible;
    public bool cantripleshoot=false;
    public bool canspeedup=false;
    public int life = 3;
    void Start() {
        Debug.Log("hello world" + name);
        Debug.Log("hello world" + transform.position.x);

       // transform.position = 


    }


        // Update is called once per frame
      void Update() {
              Movement();
        if (Input.GetKeyDown(KeyCode.Space )|| Input.GetMouseButton(0))
        {
            shoot();
           
        }

    }





    void shoot()
    {
        if (Time.time > _canfire)
        {
            if (cantripleshoot == true)
            {
                //left
                Instantiate(_trible, transform.position, Quaternion.identity);
                Debug.Log("this");
}
            else
            {
                Instantiate(_laserprefade, transform.position + new Vector3(0, 1.2f, 0), Quaternion.identity);
            }
            _canfire = Time.time + _firerate;
            

        }

    }
    void Movement()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float veticalInput = Input.GetAxis("Vertical");
        // float boun

        if (canspeedup == true)
        {
            float speedup = 5.5f;

            transform.Translate(new Vector3(1, 0, 0) * Time.deltaTime * _speed * horizontalInput* speedup);
            transform.Translate(new Vector3(0, 0.5f, 0) * Time.deltaTime * _speed * veticalInput *speedup);
            Debug.Log("now speed up");


        }
        else
        {
            transform.Translate(new Vector3(1, 0, 0) * Time.deltaTime * _speed * horizontalInput);
            transform.Translate(new Vector3(0, 0.5f, 0) * Time.deltaTime * _speed * veticalInput);

        }
        transform.Translate(new Vector3(1, 0, 0) * Time.deltaTime * _speed * horizontalInput);
        transform.Translate(new Vector3(0, 0.5f, 0) * Time.deltaTime * _speed * veticalInput);

        if (transform.position.y > 4.0f)
        {
            transform.position = new Vector3(transform.position.x, 4.0f, 0);
        }
        else if (transform.position.y < -4.0f)
        {
            transform.position = new Vector3(transform.position.x, -4.0f, 0);
        }
        if (transform.position.x > 8.0f)
        {
            transform.position = new Vector3(8.0f, transform.position.y, 0);
        }
        else if (transform.position.x < -8.0f)
        {
            transform.position = new Vector3(-8.0f, transform.position.y, 0);
        }


    }

    public void speedboot()
    {

        canspeedup = true;
        StartCoroutine(speedtime());
    }

public IEnumerator speedtime()
    {
        yield return new WaitForSeconds(5.0f);
        cantripleshoot = false;
        Debug.Log("hiiii");
    }

    public void powerupon()
    {
        cantripleshoot = true;
        StartCoroutine(poweruptime());
        Debug.Log("start");

    }


    public IEnumerator poweruptime()
    {
        yield return new WaitForSeconds(5.0f);
         cantripleshoot = false;
        Debug.Log("hiiii");
    }

    public void damage()
    {
        life--;
        if (life < 1)
        {
            Destroy(this.gameObject);
        }
    }

}
