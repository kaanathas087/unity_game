﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class laser : MonoBehaviour {
    [SerializeField]
    private float _speed = 10.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector3.up * _speed * Time.deltaTime);
        //if my laser come y greater then 5.95 i destroy the laser
         if(transform.position.y >= 5.9)
         {
             Destroy(this.gameObject);
         }
    }
}
